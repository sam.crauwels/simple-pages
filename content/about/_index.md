---
title: about
description: About me
---

Hi there! I'm Sam Crauwels, an observability engineer with Elastic and devops experience.

This is my corner of the web where I try to write things down which are relevant to me and hopefully someone else as well.

I mostly write about technical subjects, but that's not a strict rule. 

Here's a list of principles I favor and try to use:

  - [KISS (not the band)](https://en.wikipedia.org/wiki/KISS_principle)
  - [Perfect is the enemy of good](https://en.wikipedia.org/wiki/Perfect_is_the_enemy_of_good)
  - [Rubber duck debugging](https://en.wikipedia.org/wiki/Rubber_duck_debugging) 

  
Although a blog is often seen as one way communication, I would love to use it to expand my world and meet new people. So if there's a topic you'd like to discuss or if you have a comment on my post, drop me a line.

## Contact

[@Oddly](https://toot.community/@Oddly) on Mastodon
