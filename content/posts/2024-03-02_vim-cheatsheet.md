+++
title = "A personal VIM cheatsheet"
description = "Vim cheat sheet"
date = "2024-03-02T10:23:36Z"
draft= "false"
[params]
  author = "Sam"
  toc = "true"
tags = [ "vim", "shortcuts" ]
+++

This page contains a personal cheat sheet of VIM commands, keybindings or other snippets I find handy to keep around.
Because my understanding constantly changes, this page also is subject to change.

> If you want to go through a VIM tutorial on your own (Linux) system, type "vimtutor" in your terminal.


## Keys

These are keys pressed starting from the normal mode (ESC to go to normal mode) in VIM.
  
- CTRL+o: Go back to your previous cursor position.     
- CTRL+i: Go forward to your previous cursor position after using CTRL + o.  
- %: With your cursor on a bracket ( "[","(","{" ), it will bring you to the matching bracket.  
- ?: Search for the given phrase backwards.  
- /: Search for the given phrase forwards.  
- :set ignorecase: Ignore case while searching.  
- :set hlsearch: Highlight matches.  
- :set incsearch: Start searching while typing.  
- ^: Move the cursor to the first character in the beginning of the current line.  
- 0: Move the cursor to the absolute beginning of the current line.  
- n: While searching, search for the next match forwards.  
- N: While searching, search for the next match backwards.  
- p: Paste text currently in the register (after deleting or yanking) below current line.  
- R: Replace the characters starting from the cursor until "ESC" key pressed.  
- u: Undo previous change.  
- U: Undo all changes to the current line.  
- v: Start visual selection  
- yw: Yank (copy) a word.  
- yy: Yank a line.  
- zt: Scroll the current line to the top of the screen.  
- SHIFT+v: Start visual selection in line mode.  
- After this, you can delete whole lines with an operator, like "d" (delete).  
- After thise, save selected files with ":w <filename>".  
CTRL+v: Start visual selection in block mode.  
- After this, you can delete text you selected with the "x" operator.  
- You can also insert text after selecting after pressing keys "SHIFT+i" and the "ESC" key. Text will be duplicated on each line.  
- cc: Delete the line and go into insert mode.  
- ce: Delete the word and go into insert mode.  
- d2w: Delete the next two words.  
- 2dd: Delete 2 lines, the current and next line. Alternative: "d2d".  
- 2e: Move the cursor to the end of the second word forward.  
- 2w: Move the cursor to the beginning of the second word forward.  
- :s/old/new: Replace "old" string with "new" string on the current line.  
- :s/old/new/g: Replace "old" string with "new" string in the current file.  
- :s/old/new/gc: Replace "old" string with "new" string in the current file, prompting for each replacement.  
- :#,#s/old/new/g: Replace "old" string with "new" string between specific line numbers, "#,#".  
- :!ls: Run the "ls" command in the underlying terminal.  
- :r !ls: Run the "ls" command in the underlying terminal and place the output under the cursor.  
- :r \<filepath\>: Place the contents of the filepath under the cursor. Can be a absolute or relative path.  
- :\<linenumber\> + G: Go to the line number you've entered.   
- :set ruler: Show positional information in the bottom right corner.  
